/**
  * Created by lucacervello on 23/01/2017.
  */

import java.io.FileWriter

import scala.util.parsing.combinator._
import scala.collection.mutable._
import scala.io.Source
import scala.util.Try

object Util {
  import java.io.File
  def remove(filename: String): Boolean = Try(new File(filename).delete()).getOrElse(false)
  def rename(nameOld: String, nameNew: String): Boolean =
    Try(new File(nameOld).renameTo(new File(nameNew))).getOrElse(false)
  def backup(file1: String, file2: String): Boolean = Try {
    val original = new File(file1)
    val backup = new File(file2)
    val fileWriter = new FileWriter(backup)
    val originalText = Source.fromFile(original).mkString
    fileWriter write originalText
    true
  }.getOrElse(false)
  def merge(file1: String, file2:String, file3:String): Boolean = Try {
    val text1 = Source.fromFile(new File(file1)).mkString
    val text2 = Source.fromFile(new File(file2)).mkString
    new FileWriter(new File(file3)) write (text1 + text2)
    true
  }.getOrElse(false)
}

class LogLangParser extends JavaTokenParsers {
//  private def updateParserMap(id: String, value: Boolean): Unit = {
//    parserMap.get(id) map { l => parserMap.update(id, l += value)}
//  }

  def program = rep1(task)
  def task = "task" ~> ident ~ ("{" ~> taskBlock <~ "}") //^^ {_ => parserMap.put(idTask.toString, MutableList())}
  def taskBlock = rep1(command)
  def command = removeCommand | backupCommand | mergeCommand | renameCommand
  def filename = stringLiteral ^^ {s => s.toString}
  def removeCommand = "remove" ~> file ^^ { Util.remove(_) }
  def backupCommand = "backup" ~> file ~ file ^^ {
    case a ~ b => Util.backup(a, b)
  }
  def renameCommand = "rename" ~> file ~ file ^^ {
    case a ~ b => Util.rename(a, b)
  }
  def mergeCommand = "merge" ~> file ~ file ~ file ^^ {
    case a ~ b ~ c => Util.merge(a, b, c)
  }
  def file = stringLiteral ^^ { case s => s.substring(1, s.length-1) }
  def idTask = stringLiteral
}

object LogLang {
  def main(args: Array[String]) = {
    val p = new LogLangParser

    args foreach { filename  =>
      val src = scala.io.Source.fromFile(filename)
      val lines = src.mkString
      println(lines)
      p.parseAll(p.program, lines) match {
        case p.Success(result, _) => {
          println(result)
          result.foreach {
            _ match {
              case p.~(s1,l) => {
                println("Task " + s1);
                l.zipWithIndex.foreach{ case(e, i) => println(" [op" + (i + 1) + "] " + e)}
              }
            }
          }
        }
        case x => print(x.toString)
      }
    }
  }
}
