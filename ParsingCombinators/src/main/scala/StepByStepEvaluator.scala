/**
  * Created by lucacervello on 22/01/2017.
  */
import scala.annotation.tailrec
import scala.util.parsing.combinator._

trait Expr {
  def eval: Int
}

case class Cos(n: Int) extends Expr {
  def eval = n
  override def toString: String = "%d".format(n)
}

case class Sum(a: Expr, b: Expr) extends Expr {
  def eval = a.eval + b.eval
  override def toString: String = "%d + %d".format(a, b)
}

case class Minus(a: Expr, b: Expr) extends Expr {
  def eval = a.eval - b.eval

  override def toString: String = "%d - %d".format(a, b)
}

case class Div(a :Expr, b: Expr) extends Expr {
  def eval = a.eval / b.eval

  override def toString: String = "%d / %d".format(a, b)
}

case class Mul(a: Expr, b: Expr) extends Expr {
  def eval = a.eval * b.eval

  override def toString: String = "%d * %d".format(a, b)
}

object ReductionUtil {
  def eval(e: Expr): Expr = e match {
    case Cos(n) => Cos(n)
    case Sum(Cos(a), Cos(b)) => Cos(a + b)
    case Sum(a, b) => Sum(eval(a), eval(b))
    case Minus(Cos(a), Cos(b)) => Cos(a - b)
    case Minus(a, b) => Minus(eval(a), eval(b))
    case Mul(Cos(a), Cos(b)) => Cos(a * b)
    case Mul(a, b) => Mul(eval(a), eval(b))
    case Div(Cos(a), Cos(b)) => Cos((a/b).toInt)
    case Div(a, b) => Div(eval(a), eval(b))
  }

  @tailrec
  def printExpr(e: Expr) {
    println(e)
    e match {
      case Cos(n) =>
      case _ => printExpr(eval(e))
    }
  }
}

class ArithmeticParser extends RegexParsers {
  //((2 + 7) + ((3 + 9) + 4))
  def program = expr

  def expr = int | sum | minus | div | mul

  def int: Parser[Cos] = regex("""\d+""".r).map(s => Cos(s.toInt))

  def sum: Parser[Sum] = ("(" ~> expr ~ "+" ~ expr <~ ")").map { case (a ~ _ ~ b) => Sum(a, b) }

  def minus: Parser[Minus] = ("(" ~> expr ~ "-" ~ expr <~ ")").map { case (a ~ _ ~ b) => Minus(a, b) }

  def div: Parser[Div] = ("(" ~> expr ~ "/" ~ expr <~ ")").map { case (a ~ _ ~ b) => Div(a, b) }

  def mul: Parser[Mul] = ("(" ~> expr ~ "*" ~ expr <~ ")").map { case (a ~ _ ~ b) => Mul(a, b) }

}

object Evaluator {
  def main(args: Array[String]) = {
    val p = new ArithmeticParser

    args foreach { expression =>
      p.parseAll(p.expr, expression) match {
        case p.Success(result: Expr, _) => ReductionUtil.printExpr(result); println
        case x => sys.error("Could not parse the input string: " + x.toString)
      }

    }
  }
}
