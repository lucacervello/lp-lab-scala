/**
  * Created by lucacervello on 09/01/2017.
  */

import org.scalatest._

class FunctionalScalaSpec extends FlatSpec with Matchers {
  it should "recognize palindrome " in {
    FunctionalScala.is_palindrome("detartrated") should be (true)
    FunctionalScala.is_palindrome("Do geese see God?") should be (true)
    FunctionalScala.is_palindrome("Rise to vote, sir.") should be (true)
  }

  it should "recognize anagram" in {
    val dict_string = List("anna")
    FunctionalScala.isAnAnagram("nana", dict_string) should be (true)

  }

  it should "create all the prime numbers of a number" in {
    pending
    FunctionalScala.factors(5) should be (List(2, 3))
  }

  it should "is a perfect number ? " in {
    pending
    FunctionalScala.isProper(6) should be (true)
  }

}
