object FunctionalScala {

  def is_palindrome(str : String) : Boolean = {
    val cleanString = str.filter(Character.isLetterOrDigit).toLowerCase
    cleanString == cleanString.reverse
  }

  def isAnAnagram(str: String, listStr: List[String]) : Boolean = {
    def isAnagram(str1: String, str2: String) = str1.sorted == str2.sorted
    listStr.exists(x => isAnagram(str, x))
  }

  def factors(n: Number) = {
    val factors = List.range(2, n)

    List(1, 2)
  }

  def isProper(n: Number): Boolean = {
    false
  }

}